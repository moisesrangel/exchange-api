# Currency Exchange Microservice

_Currency Exchange Microservice__


### Prerequisites 📋

_All the necessary libraries are listed in the requirements.txt file_

> Python 3.8, pip

```
apt-get install python-pip
pip3 install -r requirements.txt
```

## Deployment 📦


_To create the development environment_

```
python3.8 main.py
```

## Testing 📦

Las URLS disponibles:

OpenAPI Spec:

You can see the docs in __api.yml__

https://app.swaggerhub.com/apis/blackszandor/exchange-api/1.0.0#/

Run Testing

```
pytest -s
```

## Docker 🐳

docker build -t exchangerateapi:1.0 .







