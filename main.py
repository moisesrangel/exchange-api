from flask import Flask, make_response, jsonify
from flask_cors import CORS
from flask_expects_json import expects_json, ValidationError
from modules.setup import Setup, Stock
from routes.exchange import exchange_blueprint

app = Flask(__name__)
app.register_blueprint(exchange_blueprint)
CORS(app)

@app.errorhandler(400)
def bad_request(error):
    if isinstance(error.description, ValidationError):
        original_error = error.description
        return make_response(jsonify({'error': original_error.message}), 400)
    return error


@app.before_first_request
def setup():
    '''Gets the enviroment vars from settings table'''
    Setup.create_instance()

if __name__ == '__main__':
    app.run(threaded=False,host='0.0.0.0', port=8080, debug=False)