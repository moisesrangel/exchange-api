from datetime import datetime, timedelta
from pprint import pprint

import collections
import multiprocessing
import random
import uuid

Msg = collections.namedtuple('Msg', ['event', 'args'])


class Singleton(type):
    _instances = {}
    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Stock(metaclass=Singleton):
    pass


class Quotes(metaclass=Singleton):
    pass


class Setup(object):

    @staticmethod
    def create_instance():
        Stock.currencies = ['AUD', 'CAD', 'CHF', 'EUR', 'GBP', 'JPY', 'NZD', 'USD']

        Stock.Q = Q()
        Stock.Q.start()

        for currency in Stock.currencies:
            fee = random.randint(5, 25)

            # in cents | percent
            settings = {'amount' : 1000 * 100, 'fee' : fee, 'track_fee' : 0}

            # rates
            rates = {}
            for crate in Stock.currencies:
                if crate != currency:
                    rates[crate] = random.randint(1, 10)

            settings['rates'] = rates
            setattr(Stock, currency, settings)

            print('')
            print('========================================')
            print(f'Settings for {currency}')
            pprint(getattr(Stock, currency) )
            print('========================================')


class ExchangeQuote:

    def __init__(self, currency_from:str, currency_to:str):
        super(ExchangeQuote, self).__init__()
        self.currency_from = currency_from.upper()
        self.currency_to = currency_to.upper()


    def quote(self) -> dict:
        id = uuid.uuid4()
        currency_from_object = getattr(Stock, self.currency_from, None)
        currency_to_object = getattr(Stock, self.currency_to, None)

        if currency_to_object is None:
            raise Exception(f'Invalid operation. Currency {self.currency_to} no available in settings')

        if currency_from_object is None:
            raise Exception(f'Invalid operation. Currency {self.currency_from} no available in settings')

        rate = currency_from_object.get('rates').get(self.currency_to, None)
        if rate is None:
            raise Exception(f'Invalid operation. Currency {self.currency_to} no available in settings')

        the_quote = {
            'id' : str(id),
            'exchange_rate' : rate,
            'fee_cost' : currency_from_object.get('fee', None),
            'fee_currency' : self.currency_from,
            'currency_from' : self.currency_from,
            'currency_to' : self.currency_to,
            'available_amount' : currency_to_object.get('amount', None),
            'timestamp_ms' :  int(datetime.now().timestamp() * 1000)
        }

        setattr(Quotes, str(id), the_quote)
        print("")
        print("The Quote: ")
        pprint(getattr(Quotes, str(id)) )
        return the_quote


class ExchangeCurrency:

    def __init__(self, quote_id:str, amount_cents:int):
        self.quote_id = quote_id
        self.amount_cents = amount_cents
        self.quote = getattr(Quotes, quote_id, None)

        if self.quote is None:
            raise Exception('Unavailable quote')


    def __update_stock(self, out_currency):
        # get stock in
        currency_to = getattr(Stock, self.quote.get('currency_to'))

        # get stock out
        currency_from = getattr(Stock, self.quote.get('currency_from'))

        # Convert in currency to out currency
        exchange_rate = (currency_from.get('rates').get(self.quote.get('currency_to'))) * 100
        out_currency = (self.amount_cents / exchange_rate) * 100

        # update stock in
        to_amount = currency_to.get('amount')
        to_amount = to_amount - out_currency
        currency_to['amount'] = to_amount
        pprint(getattr(Stock, self.quote.get('currency_to')))

        # update stock out
        from_amount = currency_from.get('amount')
        from_amount = from_amount + self.amount_cents
        currency_from['amount'] = from_amount
        pprint(getattr(Stock, self.quote.get('currency_from')))

        # update fee
        fee_cost = currency_to.get('fee')
        total_fee = ((self.amount_cents * fee_cost) / 100 ) / 100 # convert to units
        currency_to['track_fee'] = currency_to['track_fee'] + total_fee


    def __validate_amount(self):
        currency_to = getattr(Stock, self.quote.get('currency_to'))
        currency_from = getattr(Stock, self.quote.get('currency_from'))

        exchange_rate = (currency_from.get('rates').get(self.quote.get('currency_to'))) * 100
        out_currency = (self.amount_cents / exchange_rate) * 100

        if(out_currency > currency_to.get('amount')):
            raise Exception('Not enough')

        return out_currency


    # Quotes expires at 5 minutes
    def __validate_threshold_timestamp(self):
        now = int(datetime.now().timestamp() * 1000);
        future = datetime.fromtimestamp(self.quote.get('timestamp_ms') / 1000) + (timedelta(minutes=5)) * 1000
        future = int(future.timestamp() * 1000)

        if now > future:
            raise Exception('Quote is expired')


    def exchange(self)->dict:
        self.__validate_threshold_timestamp()
        out = self.__validate_amount()
        self.__update_stock(out)

        # return operation
        return out


class QProcess(multiprocessing.Process):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.queue = multiprocessing.Queue()
        self.queue = []

    def send(self, event, *args):
       msg = Msg(event, args)
       # self.queue.put(msg)
       self.queue.append(msg)


    def dispatch(self, msg):
        event, args = msg
        handler = getattr(self, "execute_%s" % event, None)
        if not handler:
            raise NotImplementedError(event)
        handler(*args)


    def run(self):
        while True:
            if len(Stock.Q.queue) > 0:
                msg = Stock.Q.queue[0]
                self.dispatch(msg)



class Q(QProcess):
    def execute_exchangerate(self, quote_id, amount):
        ExchangeCurrency(quote_id, amount).exchange()










