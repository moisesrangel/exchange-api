import multiprocessing
import collections

Msg = collections.namedtuple('Msg', ['event', 'args'])

class QProcess(multiprocessing.Process):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.queue = multiprocessing.Queue()
        self.queue = []

    def send(self, event, *args):
       msg = Msg(event, args)
       self.queue.append(msg)


    def dispatch(self, msg):
        event, args = msg
        handler = getattr(self, "execute_%s" % event, None)
        if not handler:
            raise NotImplementedError(event)
        handler(*args)


    def run(self):
        while True:
            if len(self.queue) > 0:
                msg = self.queue[0]
                self.dispatch(msg)