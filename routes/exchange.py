from flask import Flask, request, Blueprint, jsonify, abort
from flask_cors import CORS
from flask_expects_json import expects_json
from modules.setup import Setup, Stock, ExchangeQuote, ExchangeCurrency, Q

import datetime

exchange_blueprint = Blueprint('exchange', __name__)
schema = {
    'type': 'object',
    'properties': {
        'quote_id': {'type': 'string'},
        'amount': {'type': 'number'},
    },
    'required': ['quote_id', 'amount']
}


@exchange_blueprint.route('/exchange/rate', methods = ['GET'])
def exchange_rate():
    for param in ['currency_to', 'currency_from']:
        if param not in request.args:
            return jsonify({'error' : f'The param {param} is mandatory.'}), 400

        if request.args[param] not in Stock.currencies:
            return jsonify({'error' : f'The currency {request.args[param]} are not available 😪.'}), 400

    try:
        quote = ExchangeQuote(request.args['currency_to'] , request.args['currency_from']).quote()
        return jsonify(quote)
    except Exception as e:
        return jsonify({'error' : str(e)}), 500


@exchange_blueprint.route('/exchange/currency', methods = ['POST'])
@expects_json(schema)
def exchange_currency():
    try:
        data = request.json
        quote_id = data['quote_id']
        amount = data['amount']
        #Stock.Q.send('exchangerate', quote_id, amount)

        stock = ExchangeCurrency(quote_id, amount).exchange()
        return jsonify(stock)

    except Exception as e:
        return jsonify({'error' : str(e)}), 500



