import sys
sys.path.append('/mnt/c/apps/exchange-api')

from pprint import pprint
from modules.setup import Setup, Stock, ExchangeQuote, ExchangeCurrency

import pytest

class TestExchange:

    def test_one(self):
        Setup.create_instance()
        quote = ExchangeQuote('GBP', 'USD').quote()
        test = ExchangeCurrency(quote.get('id'), 20*100).exchange()
        print("")
        print("**************************")
        pprint(test)
        print("**************************")




