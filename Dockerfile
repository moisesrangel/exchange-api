FROM python:3.8
RUN pip3 install pipenv
ENV PROJECT_DIR /usr/src/exchangerateapi
WORKDIR ${PROJECT_DIR}
COPY Pipfile .
COPY Pipfile.lock .
COPY . .

RUN pipenv install -r requirements.txt
RUN pipenv install --deploy --ignore-pipfile

EXPOSE 8080

CMD ["pipenv", "run", "python", "main.py"]